import FoodServ from '../services/FoodServ.ts';
import { RouterContext } from "https://deno.land/x/oak/mod.ts";


const foodServ = new FoodServ();

export const getFood =  async(Context: RouterContext) => {
    Context.response.body = await foodServ.getFood();
}