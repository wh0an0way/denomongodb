import db from '../database/db.ts'
class foodRepo {

    constructor() { }

readonly foodCol = db.collection('foodR');

    async find() {
        const recipes = await this.foodCol.find();
        return recipes;
    }
}

export default foodRepo;