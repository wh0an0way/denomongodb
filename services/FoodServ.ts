import FoodRepo from '../repos/food.ts'

class FoodServ {

    constructor() { }

    readonly foodrepo = new FoodRepo();

    getFood = async () => {
        return await this.foodrepo.find();
    }

}

export default FoodServ;