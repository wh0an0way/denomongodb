import { Application, Context } from "https://deno.land/x/oak/mod.ts";
import router from './router.ts';
 
const app = new Application();

app.use((ctx) => {
  ctx.response.body = "Hello World!";
});

app.use(router.routes());


console.log("🚀 at 8000");

await app.listen({ port: 8000 });
