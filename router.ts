import { Router } from 'https:///deno.land/x/oak/mod.ts'
import { getFood } from './controllers/foodcontrol.ts'


const router = new Router();

router.get('/foodTruck', getFood)



export default router;